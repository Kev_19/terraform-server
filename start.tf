#How to create an EC2 instance,deploy it on custom VPC on a custom subnet and assign it a public IP address, 
#so that not only can we SSH to it and can access it from outside the VPC, 
#we can automatically set up a web server to run on it so that we can handle web trafiic


#EC2 is a cloud computing service that provides you
#with a virtual machine that you can use to run your applications.
#Commonly used as a web hosting service.
#Instances are nothing but servers that are running on a cloud


provider "aws" {
  access_key = "AKIAYDX4WU3VOCS6MBIV"
  secret_key = "rQonuOB2TbbsyEKRWYY8VWTbaNAmkLh1hryS6kbt"
  region     = "us-east-2"
}

#Before we get started first thing that we need to do is create a new key pair, 
#which is a private key that is used to encrypt the data that is sent to the cloud.
#ppk>>for windows OS
#pem>>for linux OS, MAC OS but we change it to ppk using putty

#1. Create VPC

resource "aws_vpc" "prod-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
        Name = "production"
    }
}

#2. Create Internet Gateway

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.prod-vpc.id

}

#3. Create Custom Route Table
#This is a custom route table that we will use to route traffic created from the subnet to the internet gateway.

resource "aws_route_table" "prod-rt" {
  vpc_id = aws_vpc.prod-vpc.id
#To do default route, change the cidr block to "0.0.0.0/0"
#Default route means that the traffic will be routed to the internet gateway
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"     #"::/0" is the default
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "Production Route Table"
  }
}

#4. Create Subnet

#A subnet is a range of IP addresses in your VPC.
#It is a logical grouping of IP addresses that you can use to create instances in your VPC.

variable "subnet_prefix" {
  description = "cidr block for the subnet"
  #default     = "10.0.0.0/24" 
  #type        = string  
}

#this is a subnet where our servers will be created
# "any " agrument is use when we dont know the value or type of the variable
resource "aws_subnet" "subnet-1" {
  vpc_id     = aws_vpc.prod-vpc.id
  cidr_block = var.subnet_prefix
  availability_zone = "us-east-1a"
 
  tags = {
    Name = "Prod-Subnet-1"
  }
}

#5. Associate Route Table with Subnet
#Route table association is used just to
#make sure that the route table is associated with the subnet.
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.prod-rt.id
}

#6. Create Security Group to allow port 22,80,443
#This is a security group that will allow traffic to the port 22,80,443
#security group is used to control the incoming traffic
resource "aws_security_group" "allow_web" {
  name        = "allow_web_traffic"
  description = "Allow Web inbound traffic"
  vpc_id      = aws_vpc.prod-vpc.id

#ingress can be used to allow traffic from the internet
#ingress dictates the traffic that is allowed to the security group
  ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] #cidr block here clamp down on what subnets can access the port
    #since we are creating a web server that is meant to use by everyone, we need to allow all traffic n hence 0.0.0.0/0
  }

  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80 #http resides on port 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
#egresss direction is used to allow traffic from the subnet to the internet
  egress {              # Deny all outbound traffic
    from_port        = 0
    to_port          = 0
    protocol         = "-1"    # -1 is used to allow all protocols
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_web"
  }
}

#7. Create a network interface with an ip in the subnet that was created in step 4
#This is a network interface that will be used to create the instance and private ip address for the host
#private ip is used to create the instance

resource "aws_network_interface" "web-server-nic" {   
  subnet_id       = aws_subnet.subnet-1.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_web.id]

}

#8. Assign an elastic IP to the network interface
#Elastic IP is public IP address that is assigned to the network interface

resource "aws_eip" "one" {
  vpc                       = true
  instance         = aws_instance.web-server-instance.id
  associate_with_private_ip = "10.0.1.50"
  depends_on = [aws_internet_gateway.gw] 
  #AWS EIP relies on deployment of Internet Gateway
  #depends_on is used to make sure that the internet gateway is deployed before the elastic ip
  # and here we will not use .id since we want the entire object
}

#9. Create Ubuntu server and install/enable apache2
#ami is the image that we will use to create the instance
resource "aws_instance" "web-server-instance" {
  ami= "ami-0e472ba40eb589f49" #Grab AMI from console
  instance_type = "t2.micro"
  availability_zone = "us-east-1a"
  key_name = "main-key" #to refrence the key pair we created earlier
#always specify the availability zone whenever get the option and use the same availability zone

  network_interface {
    device_index = 0
    #device_index is used to specify the network interface
    network_interface_id = aws_network_interface.web-server-nic.id
  }
  user_data = <<-EOF
  #!/bin/bash
  sudo apt update -y
  sudo apt install apache2 -y
  sudo systemctl start apache2
  sudo bash -c 'echo "Hello World" > /var/www/html/index.html'
  EOF

#User data is a helpful tool to get rid of routine operations after server provisioning.
#You can get a ready-to-use server with additional software installed and configured according to your specification.
#User data is a script that is executed after the instance is launched.
   
  tags = {
      Name = "web-server"
  }  
}

#print all the outputs

output "server_public_ip" {
  value = aws_eip.one.public_ip
}

output "server_private_ip" {
    value = aws_instance.web-server-instance.private_ip
}

output "server_id" {
  value = aws_instance.web-server-instance.id
}

#terraform destroy -target aws_instance.web-server-instance
# By using the -target flag, terraform will only destroy the resources that are specified in the target

